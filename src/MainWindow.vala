/*
    Copyright (c) 2015 - 2025, Vinari Software
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

using Gtk;
using App.Widgets;

namespace GUI{
	public class MainWindow : Gtk.ApplicationWindow{
		const ushort MD5=7894;													//Constant value for MD5 checksum
		const ushort SHA1=7092;													//Constant value for SHA1 checksum
		const ushort SHA256=7093;												//Constant value for SHA256 checksum
		const ushort SHA384=7094;												//Constant value for SHA384 checksum
		const ushort SHA512=7095;												//Constant value for SHA512 checksum

		private Gtk.Image resultImage;

		private Gtk.HeaderBar headerBar;
		private Gtk.MenuButton menuButton;

		private Gtk.Label firstFileLabel;
		private Gtk.Label secondFileLabel;

		private Gtk.TextView firstFileTextView;
		private Gtk.ScrolledWindow firstScrolledWindow;
		private Gtk.TextView secondFileTextView;
		private Gtk.ScrolledWindow secondScrolledWindow;

		private Gtk.Button copyFirstHashButton;
		private Gtk.Button copySecondHashButton;

		private Gtk.Button checkButton;
		private Gtk.Button resetButton;

		private Gtk.PopoverMenu popMenu;
		private Gtk.Button openFileButton;

		private App.Widgets.MessageDialog msgBox;
		private Gtk.FileChooserNative fileChooser;

		private int verificationSUM;
		private HashFunctions hash;
		private GLib.Settings mySettings;

		public MainWindow (Gtk.Application app) {
			Object (application: app);
		}

		construct {
			// The boxes that will build the GUI are set-up
			Gtk.Box mainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 28);
			Gtk.Box topFileBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 12);
			Gtk.Box bottomFileBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 12);
			Gtk.Box resultButtonBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box firstFileBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Box secondFileBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);

			// The placeholder image and it's size are configured
			resultImage=new Gtk.Image.from_resource("/org/vinarisoftware/integritycheck/../Assets/PlaceHolder.png");
			resultImage.set_pixel_size(48);

			// The HashFunctions class, and GLib.Settings are initiated
			hash=new HashFunctions();
			mySettings=new GLib.Settings("org.vinarisoftware.integritycheck");

			// Default values for the application are set-up here
			this.verificationSUM=mySettings.get_int("verification-selected");

			// Setup of the header bar that will be used in the MainWindow
			headerBar=new Gtk.HeaderBar();
			headerBar.set_show_title_buttons(true);

			// Setup of the Open file button that will be placed in the header bar
			openFileButton=new Gtk.Button.from_icon_name ("folder-open-symbolic");

			// Setup of the Menu button that will be placed in the header bar
			menuButton=new Gtk.MenuButton();
			menuButton.set_icon_name("open-menu-symbolic");

			// Menu & Open file buttons are added to the header bar
			headerBar.pack_start(openFileButton);
			headerBar.pack_end(menuButton);

			mainBox.set_margin_top(10);
			mainBox.set_margin_bottom(10);
			mainBox.set_margin_start(10);
			mainBox.set_margin_end(10);

			firstFileLabel=new Gtk.Label(_("Type the verification sum you got from the file:"));
			firstFileTextView=new Gtk.TextView();
			firstScrolledWindow=new Gtk.ScrolledWindow();

			secondFileLabel=new Gtk.Label(_("Type the verification sum of the file that should be:"));
			secondFileTextView=new Gtk.TextView();
			secondScrolledWindow=new Gtk.ScrolledWindow();

			firstFileTextView.set_monospace(true);
			firstFileTextView.set_cursor_visible(true);
			firstFileTextView.set_accepts_tab(false);
			firstFileTextView.set_wrap_mode(Gtk.WrapMode.CHAR);
			firstFileTextView.set_top_margin(7);
			firstFileTextView.set_bottom_margin(7);
			firstFileTextView.set_right_margin(7);
			firstFileTextView.set_left_margin(7);
			firstFileTextView.set_input_hints(Gtk.InputHints.NO_SPELLCHECK);
			firstFileTextView.set_input_hints(Gtk.InputHints.NO_EMOJI);

			secondFileTextView.set_monospace(true);
			secondFileTextView.set_cursor_visible(true);
			secondFileTextView.set_accepts_tab(false);
			secondFileTextView.set_wrap_mode(Gtk.WrapMode.CHAR);
			secondFileTextView.set_top_margin(7);
			secondFileTextView.set_bottom_margin(7);
			secondFileTextView.set_right_margin(7);
			secondFileTextView.set_left_margin(7);
			secondFileTextView.set_input_hints(Gtk.InputHints.NO_SPELLCHECK);
			secondFileTextView.set_input_hints(Gtk.InputHints.NO_EMOJI);

			firstScrolledWindow.set_has_frame(true);
			secondScrolledWindow.set_has_frame(true);

			firstScrolledWindow.set_hexpand(true);
			secondScrolledWindow.set_hexpand(true);

			copyFirstHashButton=new Gtk.Button.from_icon_name("edit-copy-symbolic");
			copySecondHashButton=new Gtk.Button.from_icon_name("edit-copy-symbolic");

			firstScrolledWindow.set_child(firstFileTextView);
			secondScrolledWindow.set_child(secondFileTextView);

			firstFileBox.append(firstScrolledWindow);
			firstFileBox.append(copyFirstHashButton);
			secondFileBox.append(secondScrolledWindow);
			secondFileBox.append(copySecondHashButton);

			topFileBox.append(firstFileLabel);
			topFileBox.append(firstFileBox);

			bottomFileBox.append(secondFileLabel);
			bottomFileBox.append(secondFileBox);

			checkButton=new Gtk.Button.with_label(_("Compare sums."));
			resetButton=new Gtk.Button.with_label(_("New comparison."));

			checkButton.set_hexpand(true);
			resetButton.set_hexpand(true);

			resultButtonBox.append(checkButton);
			resultButtonBox.append(resetButton);
			resultButtonBox.set_margin_bottom(20);

			mainBox.append(resultImage);
			mainBox.append(topFileBox);
			mainBox.append(bottomFileBox);
			mainBox.append(resultButtonBox);

			// Setup the Gtk.PopoverMenu with the respective buttons and actions.
			GLib.Menu mainMenu=new GLib.Menu();
			mainMenu.append(_("Preferences"), "app.preferences");
			mainMenu.append(_("About"), "app.about");
			mainMenu.append(_("Quit"), "app.quit");

			popMenu=new Gtk.PopoverMenu.from_model(mainMenu);
			menuButton.set_popover(popMenu);

			// Signals and actions will be connected from here
			this.close_request.connect(beforeDestroy);

			resetButton.clicked.connect(resetButtonAction);
			checkButton.clicked.connect(checkButtonAction);
			openFileButton.clicked.connect(openFileButtonAction);

			copyFirstHashButton.clicked.connect(copyFirstHashButtonAction);
			copySecondHashButton.clicked.connect(copySecondHashButtonAction);

			//Handles the menubar and the icon assigned to this window
			this.set_handle_menubar_accel(true);
			this.set_titlebar(headerBar);
			this.set_icon_name("org.vinarisoftware.integritycheck");
			this.set_title("Integrity Check - Vinari Software");

			//Handles the size and state of this window
			this.set_resizable(false);
			this.set_default_size(340, 400);

			//Presents the widgets of this window, and the window!
			this.set_child(mainBox);
			// this.present() Is not needed since that function is called from 'Application.vala'
		}

		private bool beforeDestroy(){
			return false;
		}

		private void copyFirstHashButtonAction(){
			firstFileTextView.select_all(true);
			firstFileTextView.copy_clipboard();
			firstFileTextView.select_all(false);
		}

		private void copySecondHashButtonAction(){
			secondFileTextView.select_all(true);
			secondFileTextView.copy_clipboard();
			secondFileTextView.select_all(false);
		}

		private void resetButtonAction(){
			firstFileLabel.set_text(_("Type the verification sum you got from the file:"));					// Set the default value for the first file label
			secondFileLabel.set_text(_("Type the verification sum of the file that should be:"));			// Set the default value for the second file label

			firstFileTextView.set_editable(true);															// Enable the first file Gtk.TextView
			secondFileTextView.set_editable(true);															// Enable the second file Gtk.TextView

			firstFileTextView.buffer.text="";																// Clear the text on the first file Gtk.TextView
			secondFileTextView.buffer.text="";																// Clear the text on the second file Gtk.TextView

			openFileButton.set_sensitive(true);																// Enable the button that opens the Gtk.FileChooserNative

			checkButton.set_sensitive(true);																// Enable the check Gtk.Button
			resultImage.set_from_resource("/org/vinarisoftware/integritycheck/../Assets/PlaceHolder.png");	// The image that displays the check or cross will be set to a placeHolder
		}

		private void checkButtonAction(){
			string sum1=firstFileTextView.buffer.text;
			string sum2=secondFileTextView.buffer.text;

			if(sum1=="" || sum2==""){
				msgBox=new App.Widgets.MessageDialog(this, ERROR, OK, _("One of the two text fields is empty.\nThe verification can not be done."));
				return;
			}

			if(firstFileTextView.get_editable()==true){														// Checks if the first file Gtk.TextView is editable
				firstFileTextView.set_editable(false);														// If it is, the first file Gtk.TextView will be read-only
			}

			if(secondFileTextView.get_editable()==true){													// Checks if the second file Gtk.TextView is editable
				secondFileTextView.set_editable(false);														// If it is, the second file Gtk.TextView will be read-only
			}

			checkButton.set_sensitive(false);

			IntegrityChecker checker=new IntegrityChecker(sum1, sum2, mySettings.get_boolean("ignore-capitalization"));
			bool integrityOk=checker.compareHashes();

			if(integrityOk){
				resultImage.set_from_icon_name("object-select-symbolic");
				msgBox=new App.Widgets.MessageDialog(this, INFO, OK, _("The verification sums match perfectly, therefore, your file is intact."));
			}else{
				resultImage.set_from_icon_name("window-close-symbolic");
				msgBox=new App.Widgets.MessageDialog(this, WARNING, OK, _("The verification sums don't match at all.\n Is possible that the file is altered, corrupted, or incomplete."));
			}
		}

		private void fileChooserAction(int response){
			fileChooser.destroy();
			GLib.File fileSelected;
			string fileSumHEX="";

			if(response==Gtk.ResponseType.ACCEPT){
				fileSelected=this.fileChooser.get_file();

				switch(this.verificationSUM){
					case MD5: fileSumHEX=hash.getSelectedSum(fileSelected, GLib.ChecksumType.MD5); break;
					case SHA1: fileSumHEX=hash.getSelectedSum(fileSelected, GLib.ChecksumType.SHA1); break;
					case SHA256: fileSumHEX=hash.getSelectedSum(fileSelected, GLib.ChecksumType.SHA256); break;
					case SHA384: fileSumHEX=hash.getSelectedSum(fileSelected, GLib.ChecksumType.SHA384); break;
					case SHA512: fileSumHEX=hash.getSelectedSum(fileSelected, GLib.ChecksumType.SHA512); break;
				}

				if(firstFileTextView.buffer.text==""){
					firstFileTextView.buffer.text=fileSumHEX;

					if(fileSelected.get_basename().length<=53){
						firstFileLabel.set_text(_("File: ")+fileSelected.get_basename());
					}

					firstFileTextView.set_editable(false);
				}else if(secondFileTextView.buffer.text==""){
					secondFileTextView.buffer.text=fileSumHEX;

					if(fileSelected.get_basename().length<=53){
						secondFileLabel.set_text(_("File: ")+fileSelected.get_basename());
					}

					secondFileTextView.set_editable(false);
					openFileButton.set_sensitive(false);

					if(mySettings.get_boolean("auto-comparation")){
						this.checkButton.clicked();
					}
				}
			}else{
				return;
			}
		}

		private void openFileButtonAction(){
			this.verificationSUM=mySettings.get_int("verification-selected");
			fileChooser=new Gtk.FileChooserNative(_("Select a file..."), this, Gtk.FileChooserAction.OPEN, _("Select file"), _("Cancel"));
			fileChooser.response.connect(fileChooserAction);
			fileChooser.show();
		}
	}
}
