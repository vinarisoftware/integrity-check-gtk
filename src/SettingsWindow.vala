/*
    Copyright (c) 2015 - 2025, Vinari Software
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace GUI{
	public class SettingsWindow : Gtk.Window {
		const ushort MD5=7894;
		const ushort SHA1=7092;
		const ushort SHA256=7093;
		const ushort SHA384=7094;
		const ushort SHA512=7095;

		private Gtk.Box mainBox;
		private Gtk.Box buttonControlBox;
		private Gtk.Box selectHashBox;
		private Gtk.Box ignoreCapitalizationBox;
		private Gtk.Box inmediateComparisonBox;

		private Gtk.ComboBoxText selectHashCombo;
		private Gtk.Switch ignoreCapitalizationSwitch;
		private Gtk.Switch inmediateComparisonSwitch;
		private Gtk.Button acceptButton;
		private Gtk.Button cancelButton;

		private Gtk.Label ignoreCapitalizationLabel;
		private Gtk.Label inmediateComparisonLabel;
		private Gtk.Label selectHashLabel;
		private GLib.Settings mySettings;

		private int selectedHash;

		public SettingsWindow(Gtk.Window parent){
			this.set_transient_for(parent);
			this.set_modal(true);
			this.set_resizable(false);
			this.set_icon_name("org.vinarisoftware.integritycheck");
			this.set_title(_("Integrity Check - Vinari Software | Preferences"));
		}

		construct{
			mySettings=new GLib.Settings("org.vinarisoftware.integritycheck");
			mainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 15);

			mainBox.set_margin_top(10);
			mainBox.set_margin_bottom(10);
			mainBox.set_margin_start(10);
			mainBox.set_margin_end(10);

			selectHashBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			selectHashLabel=new Gtk.Label(_("Select the verification sum you want to calculate."));
			selectHashCombo=new Gtk.ComboBoxText();

			selectHashBox.append(selectHashLabel);
			selectHashBox.append(selectHashCombo);

			selectHashCombo.append_text("MD5");
			selectHashCombo.append_text("SHA1");
			selectHashCombo.append_text("SHA256");
			selectHashCombo.append_text("SHA384");
			selectHashCombo.append_text("SHA512");

			this.selectedHash=mySettings.get_int("verification-selected");

			switch(this.selectedHash){
				case MD5: selectHashCombo.set_active(0); break;
				case SHA1: selectHashCombo.set_active(1); break;
				case SHA256: selectHashCombo.set_active(2); break;
				case SHA384: selectHashCombo.set_active(3); break;
				case SHA512: selectHashCombo.set_active(4); break;
				default: selectHashCombo.set_active(0); break;
			}

			ignoreCapitalizationBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			ignoreCapitalizationLabel=new Gtk.Label(_("Ignore uppercase and lowercase difference."));
			ignoreCapitalizationSwitch=new Gtk.Switch();

			ignoreCapitalizationSwitch.set_active(mySettings.get_boolean("ignore-capitalization"));

			ignoreCapitalizationBox.append(ignoreCapitalizationLabel);
			ignoreCapitalizationBox.append(ignoreCapitalizationSwitch);

			inmediateComparisonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			inmediateComparisonLabel=new Gtk.Label(_("Compare when the two fields have content."));
			inmediateComparisonSwitch=new Gtk.Switch();

			inmediateComparisonSwitch.set_active(mySettings.get_boolean("auto-comparation"));

			inmediateComparisonBox.append(inmediateComparisonLabel);
			inmediateComparisonBox.append(inmediateComparisonSwitch);

			buttonControlBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
			cancelButton=new Gtk.Button.with_label(_("Cancel"));
			acceptButton=new Gtk.Button.with_label(_("Apply changes"));

			cancelButton.set_hexpand(true);
			acceptButton.set_hexpand(true);

			buttonControlBox.append(cancelButton);
			buttonControlBox.append(acceptButton);

			selectHashLabel.set_hexpand(true);
			selectHashLabel.set_halign(Gtk.Align.START);
			ignoreCapitalizationLabel.set_hexpand(true);
			ignoreCapitalizationLabel.set_halign(Gtk.Align.START);
			inmediateComparisonLabel.set_hexpand(true);
			inmediateComparisonLabel.set_halign(Gtk.Align.START);

			Gtk.Separator separatorI=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
			Gtk.Separator separatorIII=new Gtk.Separator(Gtk.Orientation.HORIZONTAL);

			selectHashCombo.changed.connect(selectHashComboAction);

			cancelButton.clicked.connect(cancelButtonAction);
			acceptButton.clicked.connect(acceptButtonAction);

			mainBox.append(ignoreCapitalizationBox);
			mainBox.append(inmediateComparisonBox);
			mainBox.append(separatorIII);

			mainBox.append(selectHashBox);
			mainBox.append(separatorI);

			mainBox.append(buttonControlBox);
			this.set_child(mainBox);
		}

		public void run(){
			this.present();
		}

		private void cancelButtonAction(){
			this.destroy();
		}

		private void selectHashComboAction(){
			switch(selectHashCombo.get_active_text()){
				case "MD5": this.selectedHash=MD5; break;
				case "SHA1": this.selectedHash=SHA1; break;
				case "SHA256": this.selectedHash=SHA256; break;
				case "SHA384": this.selectedHash=SHA384; break;
				case "SHA512": this.selectedHash=SHA512; break;
				default: this.selectedHash=MD5; break;
			}
		}

		private void acceptButtonAction(){
			mySettings.set_int("verification-selected", this.selectedHash);
			mySettings.set_boolean("auto-comparation", inmediateComparisonSwitch.get_active());
			mySettings.set_boolean("ignore-capitalization", ignoreCapitalizationSwitch.get_active());

			this.destroy();
		}
	}
}
